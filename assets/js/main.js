$(document).ready(function() {
		 $('.owl1').owlCarousel({
                items: 4,
                loop: true,
                dots: false,
                nav: false,
                autoplay:true,
                autoplayTimeout:3000           
              });
		 $('.owl-mini').owlCarousel({
                items: 5,
                loop: true,
                dots: false,
                nav: false,
                autoplay:true,
                autoplayTimeout:3000           
              });
         $('.owl-more').owlCarousel({
                items: 4,
                loop: true,
                dots: false,
                nav: false,
                autoplay:true,
                autoplayTimeout:3000           
              });
         $('.search-input').keypress(function(e) {
                let key = $(this).val();
                if(e.which == 13) {
                   location.href="/search?q="+key;
                }
        });
        $(document).on('click','.show-menu',function(){
          $('.item-vertical li .menu-con').hide(500);
          $('.item-vertical li .show-more-menu i').removeClass('fa-minus').addClass('fa-plus');
          $('.item-vertical li .show-more-menu').addClass('show-menu').removeClass('hide-menu');
          $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
          $(this).parents('li').find('.menu-con').show('500');
          $(this).addClass('hide-menu').removeClass('show-menu');
        });
         $(document).on('click','.hide-menu',function(){
          $(this).find('i').removeClass('fa-minus').addClass('fa-plus');
          $(this).parents('li').find('.menu-con').hide('500');
          $(this).addClass('show-menu').removeClass('hide-menu');
        });
	});