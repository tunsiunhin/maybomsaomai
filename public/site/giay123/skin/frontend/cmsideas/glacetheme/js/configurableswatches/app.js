
// ==============================================
// PDP - image zoom - needs to be available outside document.ready scope
// ==============================================

var ProductMediaManager = {
    IMAGE_ZOOM_THRESHOLD: 20,
    imageWrapper: null,

    swapImage: function(targetImage) {
        targetImage = Glacetheme_1102(targetImage);
        targetImage.addClass('gallery-image');

        if(targetImage[0].complete) { //image already loaded -- swap immediately
			 Glacetheme_1102("li.etalage_thumb").trigger('zoom.destroy');
			 Glacetheme_1102("li.etalage_thumb .gallery-image").remove();
			 Glacetheme_1102("li.etalage_thumb img.etalage_thumb_image").show();
			 Glacetheme_1102("li.etalage_thumb_active img.etalage_thumb_image").hide();
			 Glacetheme_1102(targetImage).insertBefore(Glacetheme_1102("li.etalage_thumb_active img.etalage_thumb_image"));
			 imagesLoaded(targetImage, function() {
				 if(typeof zoom_enabled !== 'undefined' && zoom_enabled == true)
					 Glacetheme_1102("li.etalage_thumb").zoom({touch:false});
			 });

        } else { //need to wait for image to load
			 Glacetheme_1102("li.etalage_thumb").trigger('zoom.destroy');
			 Glacetheme_1102("li.etalage_thumb .gallery-image").remove();
			 Glacetheme_1102("li.etalage_thumb img.etalage_thumb_image").show();
			 Glacetheme_1102("li.etalage_thumb_active img.etalage_thumb_image").hide();
			 Glacetheme_1102(targetImage).insertBefore(Glacetheme_1102("li.etalage_thumb_active img.etalage_thumb_image"));
            imagesLoaded(targetImage, function() {
				if(typeof zoom_enabled !== 'undefined' && zoom_enabled == true)
	                Glacetheme_1102("li.etalage_thumb").zoom({touch:false});
            });

        }
    },
    init: function() {
        ProductMediaManager.imageWrapper = Glacetheme_1102('.product-img-box');

        Glacetheme_1102(document).trigger('product-media-loaded', ProductMediaManager);
    }
};

Glacetheme_1102(document).ready(function() {
    ProductMediaManager.init();
});