var easyajaxcart_timer;
var easyajaxcart_sec;
Glacetheme_1102.noConflict();
	function setAjaxData(data,iframe,type){
		if(data.status == 'ERROR'){
			alert(data.message.replace("<br/>",""));
		}else{
            if(Glacetheme_1102('.header .mini-cart')){
                Glacetheme_1102('.header .mini-cart').replaceWith(data.toplink);
            }
            if(Glacetheme_1102('.fixed-header .mini-cart')){
                Glacetheme_1102('.fixed-header .mini-cart').replaceWith(data.toplink);
            }
            if(Glacetheme_1102('.sticky-header .mini-cart')){
                Glacetheme_1102('.sticky-header .mini-cart').replaceWith(data.toplink);
            }
            if(Glacetheme_1102('.col-right .block.block-cart')){
                Glacetheme_1102('.col-right .block.block-cart').replaceWith(data.cart_sidebar);
            }
			Glacetheme_1102('#after-loading-success-message #success-message-container .msg-box').html(data.message);
	        Glacetheme_1102.fancybox.close();
			if(type!='item'){
				Glacetheme_1102('#after-loading-success-message').fadeIn(200);
                easyajaxcart_sec = Glacetheme_1102('#after-loading-success-message .timer').text();
                easyajaxcart_timer = setInterval(function(){
                    Glacetheme_1102('#after-loading-success-message .timer').html(Glacetheme_1102('#after-loading-success-message .timer').text()-1);
                },1000)
                setTimeout(function(){
                    Glacetheme_1102('#after-loading-success-message').fadeOut(200);
                    clearTimeout(easyajaxcart_timer);
                    setTimeout(function(){
                        Glacetheme_1102('#after-loading-success-message .timer').html(easyajaxcart_sec);
                    }, 1000);
                },easyajaxcart_sec*1000);
			}
		}
	}
	function setLocationAjax(url,id,type){
        if (url.indexOf("?")){
            url = url.split("?")[0];
        }
		url += 'isAjax/1';
		url = url.replace("checkout/cart","easyajaxcart/index");
        if(window.location.href.match("https://") && !url.match("https://")){
            url = url.replace("http://", "https://");
        }
        if(window.location.href.match("http://") && !url.match("http://")){
            url = url.replace("https://", "http://");
        }
		Glacetheme_1102('#loading-mask').show();

		try {
			Glacetheme_1102.ajax( {
				url : url,
				dataType : 'json',
				success : function(data) {
					Glacetheme_1102('#loading-mask').hide();
         			setAjaxData(data,false,type);
				}
			});
		} catch (e) {
		}
	}

    function showOptions(id){
		initFancybox();
        Glacetheme_1102('#fancybox'+id).trigger('click');
    }
	
	function initFancybox(){
		Glacetheme_1102.noConflict();
		Glacetheme_1102(document).ready(function(){
		Glacetheme_1102('.fancybox').fancybox({
				hideOnContentClick : true,
				width: 382,
				autoDimensions: true,
				type : 'iframe',
				showTitle: false,
				scrolling: 'no',
				onComplete: function(){
					Glacetheme_1102('#fancybox-frame').load(function() { // wait for frame to load and then gets it's height
						Glacetheme_1102('#fancybox-content').height(Glacetheme_1102(this).contents().find('body').height()+100);
						Glacetheme_1102.fancybox.resize();
					});

				}
			}
		);
		});   	
	}
	function ajaxCompare(url,id){
	    url = url.replace("catalog/product_compare/add","easyajaxcart/whishlist/compare");
		if (url.indexOf("?")){
            url = url.split("?")[0];
        }
		url += 'isAjax/1';
        if(window.location.href.match("https://") && !url.match("https://")){
            url = url.replace("http://", "https://");
        }
        if(window.location.href.match("http://") && !url.match("http://")){
            url = url.replace("https://", "http://");
        }
		Glacetheme_1102('#loading-mask').show();

	    Glacetheme_1102.ajax( {
		    url : url,
		    dataType : 'json',
		    success : function(data) {
			    Glacetheme_1102('#loading-mask').hide();
			    if(data.status == 'ERROR'){
				    alert(data.message.replace("<br/>",""));
			    }else{
				    alert(data.message.replace("<br/>",""));
				    if(Glacetheme_1102('.block-compare').length){
                        Glacetheme_1102('.block-compare').replaceWith(data.sidebar);
                    }else{
                        if(Glacetheme_1102('.col-right').length){
                    	    Glacetheme_1102('.col-right').prepend(data.sidebar);
                        }
                    }
			    }
		    }
	    });
    }
    function ajaxWishlist(url,id){
	    url = url.replace("wishlist/index","easyajaxcart/whishlist");
        if (url.indexOf("?")){
            url = url.split("?")[0];
        }
		url += 'isAjax/1';
        if(window.location.href.match("https://") && !url.match("https://")){
            url = url.replace("http://", "https://");
        }
        if(window.location.href.match("http://") && !url.match("http://")){
            url = url.replace("https://", "http://");
        }
	    Glacetheme_1102('#loading-mask').show();
	    Glacetheme_1102.ajax( {
		    url : url,
		    dataType : 'json',
		    success : function(data) {
			    Glacetheme_1102('#loading-mask').hide();
			    if(data.status == 'ERROR'){
				    alert(data.message.replace("<br/>",""));
			    }else{
				    alert(data.message.replace("<br/>",""));
                    if(Glacetheme_1102('.header > .quick-access > .links')){
                        Glacetheme_1102('.header > .quick-access > .links').replaceWith(data.toplink);
                    }
			    }
		    }
	    });
    }
    function deleteAction(deleteUrl,itemId,msg){
	    var result =  confirm(msg);
	    if(result==true){
		    setLocationAjax(deleteUrl,itemId,'item')
	    }else{
		    return false;
	    }
    }