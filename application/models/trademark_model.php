<?php
Class Trademark_model extends MY_model
{
    var $table = 'trademarks';

    public function get_all_trademarks()
    {
    	$result = $this->db->get('trademarks')->result_array();
    	return $result;
    }
}