<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
 * Lay ngay tu dang int
 * @$time : int - thoi gian muon hien thi ngay
 * @$full_time : cho biet co lay ca gio phut giay hay khong
 */
function get_date($time)
{
	date_default_timezone_set('Asia/Ho_Chi_Minh');
	
    $date = unix_to_human($time);
    return $date;
}
function public_url($url = '')
{
	return base_url('public/'.$url);
}

function admin_url($url = '')
{
    return base_url('admin/'.$url);
}
function pre($list, $exit = true)
{
    echo "<pre>";
    print_r($list);
    if($exit)
    {
        die();
    }
}

function str_slug($str, $seperator = '-') {
    $str = strip_tags($str);
    $str = trim(mb_strtolower($str,"UTF-8"));
    $str = preg_replace('/(\s+)|(\-)/', ' ', $str);
    $str = preg_replace('/(à|À|Á|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/[^a-zA-z0-9-\s]/', '', $str);
  
    $str = preg_replace('/\s+/', $seperator, $str);
    return trim($str, '-');
}

function get_date_news($time, $full_time = true)
{
    $fomat = '%d';
    $date = mdate($fomat , $time);
    return $date;
}

function get_month($time, $full_time = true)
{
    $fomat = '%m';
    $date = mdate($fomat , $time);
    return $date;
}


function get_date_full($time, $full_time = true)
{
    $fomat = '%d-%m-%Y';
    if($full_time)
    {
        $fomat = $fomat.' - %H:%i:%s';
    }
    $date = mdate($fomat , $time);
    return $date;
}

