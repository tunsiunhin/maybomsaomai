<?php

Class MY_Controller extends CI_Controller
{
    //bien gui du lieu sang ben view
    public $data = array();

    function __construct()
    {
        //ke thua tu CI_Controller
        parent::__construct();
        $this->load->library('session');

        $controller = $this->uri->segment(1);
        $controller = strtolower($controller);
        switch ($controller) {
            case 'admin' : {
                //xu ly cac du lieu khi truy cap vao trang admin
                $this->load->helper('admin');
                $this->_check_login();
                $login = $this->session->userdata('login');

                // $permission = (array)json_decode( $login['permissions']);
                // if(empty($permission))
                //     die();
                // $this->data['permissions'] = $permission;

                //lấy ra thông tin admin đăng nhập
                $this->load->model('admin_model');
                $admin_info = $this->admin_model->get_info($login['id']);
                $this->data['admin_info'] = $admin_info;

                /*
                //nếu đăng nhập vào admin rùi mà admin nhập linh tinh trên url thì redirect về trang home
                */
                $controller2 = $this->uri->rsegment(1);

                $controller2 = ucfirst(strtolower($controller2)) . '.php';
                if (!file_exists(FCPATH . 'application/controllers/admin/' . $controller2)) {
                    redirect(admin_url('home'));
                }
                break;

            }
            default: {
                $this->load->model('catalog_model');
                $input = array();
                $input['where'] = array('parent_id' => 0, 'sort_order !=' => 0);
                $input['order'] = array('sort_order', 'ASC');
                $input['limit'] = array('10' ,'0');
                $catalog_list = $this->catalog_model->get_list($input);
                foreach ($catalog_list as $row) {
                    $input['where'] = array('parent_id' => $row->id);
                    $subs = $this->catalog_model->get_list($input);
                    $row->subs = $subs;
                }
                $this->data['catalog_list'] = $catalog_list;
                }

        }
    }

    /*
     * Kiem tra trang thai dang nhap cua admin
     */
    private function _check_login()
    {
        $controller = $this->uri->rsegment('1');
        $controller = strtolower($controller);

        $login = $this->session->userdata('login');
        $this->data['login'] = $login;
        //neu ma chua dang nhap,ma truy cap 1 controller khac login
        if (!$login && $controller != 'login') {
            redirect(admin_url('login'));
        }
        //neu ma admin da dang nhap thi khong cho phep vao trang login nua.
        if ($login && $controller == 'login') {
            redirect(admin_url('home/index'));
        }
    }


    /*
    //check level của ban quản trị
    */

    function getPermission()
    {
        $id = json_decode($this->session->userdata('login')['id']);
        $admin_level = $this->admin_model->get_info($id)->level;
        return $admin_level;
    }
}


