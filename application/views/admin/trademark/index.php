<!-- head -->
<?php $this->load->view('admin/trademark/head', $this->data)?>

<div class="line"></div>

<div class="wrapper">

    <?php $this->load->view('admin/message', $this->data);?>

	<div class="widget">

		<div class="title">
			<span class="titleIcon">
			<div class="checker" id="uniform-titleCheck">
    			<span>
    			   <input type="checkbox" name="titleCheck" id="titleCheck" style="opacity: 0;">
    			</span>
			</div>
			</span>
			<h6>Danh sách thương hiệu</h6>
		 	<div class="num f12">Tổng số: <b><?php echo count($list)?></b></div>
		</div>

		<table cellpadding="0" cellspacing="0" width="100%" class="sTable mTable myTable withCheck" id="checkAll">
			<thead>
				<tr>
					<td style="width:10px;"><img src="<?php echo public_url('admin')?>/images/icons/tableArrows.png" /></td>
					<td style="width:80px;">Mã số</td>
					<td>Tên danh mục</td>
					<td style="width:100px;">Hành động</td>
				</tr>
			</thead>
			<tbody class="list_item">
			<?php foreach ($list as $value):?>
				<tr class="row_<?php echo $value->id?>">
					<td class="textC"></td>
					<td class="textC"><?php echo $value->id?></td>
					<td class="textC"><?php echo $value->name?></td>
					<td class="option textC">
						<a class="tipS" title="Chỉnh sửa" href="<?php echo admin_url('trademark/edit/'.$value->id)?>">
							<img src="<?php echo public_url('admin/images')?>/icons/color/edit.png">
						</a>

						<a class="tipS verify_action" title="Xóa" href="<?php echo admin_url('trademark/delete/'.$value->id)?>">
							<img src="<?php echo public_url('admin/images')?>/icons/color/delete.png">
						</a>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>

<div class="clear mt30"></div>
