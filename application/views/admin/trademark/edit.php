<!-- head -->
<?php $this->load->view('admin/trademark/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới thương hiệu</h6>
        </div>

        <form id="form" class="form" enctype="multipart/form-data" method="post" action="">
            <fieldset>

                <div class="formRow">
                    <label for="param_name" class="formLeft">Tên:<span class="dsadasdas">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" _autocheck="true" id="param_name"
                                                    value="<?php echo $info->name?>" name="name"></span>
                        <span class="autocheck" name="name_autocheck"></span>
                        <div class="clear error" name="name_error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_name" class="formLeft">Slug:</label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo $info->slug; ?>"
                                                    _autocheck="true" id="param_name" name="slug"></span>

                        <span class="autocheck" name="name_autocheck"></span>
                        <div class="clear error" name="name_error"><?php echo form_error('slug') ?></div>
                    </div>
                    <p class="formRight">Nhập slug nếu muốn thay đổi</p>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_meta_desc" class="formLeft">Mô tả:</label>
                    <div class="formRight">
                        <span class="oneTwo"><textarea cols="" rows="4" _autocheck="true" id="param_meta_desc"
                                                       name="description"> <?php echo $info->description; ?></textarea></span>
                        <span class="autocheck" name="meta_desc_autocheck"></span>
                        <div class="clear error" name="meta_desc_error"><?php echo form_error('meta_desc') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formSubmit">
                    <input type="submit" class="redB" value="Cập nhật">
                </div>
            </fieldset>
        </form>

    </div>
</div>
