<!-- head -->
<?php $this->load->view('admin/trademark/head', $this->data) ?>

<div class="line"></div>

<div class="wrapper">
    <div class="widget">
        <div class="title">
            <h6>Thêm mới thương hiệu</h6>
        </div>

        <form id="form" class="form" enctype="multipart/form-data" method="post" action="">
            <fieldset>

                <div class="formRow">
                    <label for="param_name" class="formLeft">Tên:<span class="dsadasdas">*</span></label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" _autocheck="true" id="param_name"
                                                    value="<?php echo set_value('name') ?>" name="name"></span>
                        <span class="autocheck" name="name_autocheck"></span>
                        <div class="clear error" name="name_error"><?php echo form_error('name') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="formRow">
                    <label for="param_name" class="formLeft">Slug:</label>
                    <div class="formRight">
                        <span class="oneTwo"><input type="text" value="<?php echo set_value('slug'); ?>"
                                                    _autocheck="true" id="param_name" name="slug"></span>

                        <span class="autocheck" name="name_autocheck"></span>
                        <div class="clear error" name="name_error"><?php echo form_error('slug') ?></div>
                    </div>
                    <p class="formRight">Nhập slug nếu muốn thay đổi</p>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <label for="param_meta_desc" class="formLeft">Mô tả:</label>
                    <div class="formRight">
                        <span class="oneTwo"><textarea cols="" rows="4" _autocheck="true" id="param_meta_desc"
                                                       name="description"></textarea></span>
                        <span class="autocheck" name="meta_desc_autocheck"></span>
                        <div class="clear error" name="meta_desc_error"><?php echo form_error('meta_desc') ?></div>
                    </div>
                    <div class="clear"></div>
                </div>


<!--                <div class="formRow">-->
<!--                    <label for="param_name" class="formLeft">Chọn vị trí hiển thị ra trang chủ:<span-->
<!--                            class="req">*</span></label>-->
<!--                    <div class="formRight">-->
<!--                    <span class="oneTwo">-->
<!--                      <select _autocheck="true" id="param_parent_id" name="status">-->
<!--                          <option value="0">Không hiển thị</option>-->
<!--                          <option value="1">vị trí 1</option>-->
<!--                          <option value="2">vị trí 2</option>-->
<!--                          <option value="3">vị trí 3</option>-->
<!--                          <option value="4">vị trí 4</option>-->
<!--                          <option value="5">vị trí 5</option>-->
<!--                          <option value="6">vị trí 6</option>-->
<!--                          <option value="7">vị trí 7</option>-->
<!--                          <option value="8">vị trí 8</option>-->
<!--                          <option value="9">vị trí 9</option>-->
<!--                      </select>-->
<!--                    </span>-->
<!--                        <span class="autocheck" name="name_autocheck"></span>-->
<!--                        <div class="clear error" name="name_error">--><?php //echo form_error('name') ?><!--</div>-->
<!--                    </div>-->
<!--                    <div class="clear"></div>-->
<!--                </div>-->
                <div class="formSubmit">
                    <input type="submit" class="redB" value="Thêm mới">
                </div>
            </fieldset>
        </form>

    </div>
</div>
