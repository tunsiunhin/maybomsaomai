<div id="leftSide" style="padding-top:30px;">

    <!-- Account panel -->

    <div class="sideProfile">
        <a href="#" title="" class="profileFace"><img src="<?php echo public_url('admin') ?>/images/user.png"
                                                      width="40"></a>
	<span>Xin chào: <strong>
            <?php if ($admin_info->level == 1) {
                echo 'admin';
            } else {
                echo 'editor';
            }
            ?>

        </strong></span>
        <span><?php echo isset($admin_info->name) ? $admin_info->name : '' ?></span>
        <div class="clear"></div>
    </div>
    <div class="sidebarSep"></div>
    <!-- Left navigation -->

    <ul id="menu" class="nav">

<!--        <li class="home">-->
<!---->
<!--            <a href="--><?php //echo admin_url('home') ?><!--" class="active" id="current">-->
<!--                <span>Trang quản trị</span>-->
<!--                <strong></strong>-->
<!--            </a>-->
<!--        </li>-->
        <li class="product">

            <a href="" class="exp inactive">
                <span>Sản phẩm</span>
                <strong>2</strong>
            </a>
            <ul style="display: none;" class="sub">
                <li>
                    <a href="<?php echo admin_url('product') ?>">
                        Sản phẩm
                    </a>
                </li>
                <li>
                    <a href="<?php echo admin_url('catalog') ?>">
                        Danh mục
                    </a>
                </li>

            </ul>

        </li>
        <li class="account">

            <a href="" class="exp inactive">
                <span>Tài khoản</span>
                <strong>1</strong>
            </a>

            <ul style="display: none;" class="sub">
                <li>
                    <a href="<?php echo admin_url('admin') ?>">
                        Ban quản trị
                    </a>
                </li>

            </ul>

        </li>
        <li class="account">

            <a href="" class="exp inactive">
                <span>Thương hiệu</span>
                <strong>1</strong>
            </a>

            <ul style="display: none;" class="sub">
                <li>
                    <a href="<?php echo admin_url('trademark') ?>">
                        Danh sách
                    </a>
                </li>

            </ul>

        </li>
    </ul>

</div>
<div class="clear"></div>
	