<div class="container-fuild" style="background: white;padding-top:40px">
	<div class="container">
	<div class="col-md-6 img-detail">
		<img class="img-responsive " src="<?php echo $product->image_link; ?>" alt="<?php echo $product->name?>">
	</div>
	<div class="col-md-6">
		<div class="main-detail">
			<div class="name-detail">
				<h1><?php echo $product->name; ?></h1>
			</div>
			<div class="price-detail">
				<span class="price-detail-main"><?php echo ($product->price != 0) ? number_format($product->price) : 'Giá Liên Hệ';?></span>
			</div>
			<div class="short-description">
				<?php echo !empty($product->content) ? $product->content : ''?>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="description-detail">
			<ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active">
                            <a href="javascript(0);" >Thông Tin Sản Phẩm</a>
                   </li>
             </ul>
             <div class="content-description-detail"><p><?php echo !empty($product->thongsokythuat) ? $product->thongsokythuat : ''?></p>

		</div>
	</div>
</div>
</div>
<div class="container">
	<div class="more-product">
		<div class="titel-more">
			<h2>Sản phẩm Liên Quan</h2>
		</div>
	</div>
	<div class="slider-more">
		<div class="owl-carousel owl-more owl-theme">
			<?php if(!empty($related_post)) { ?>
			<?php foreach ($related_post as $value) { ?>
            <div class="slide-more">
                <div class="img-more">
                	<a href="<?php echo base_url($value->slug_catalog->slug.'/'.$value->slug.'.html')?>">
                  		<img class="img-responsive img-product" src="<?php echo $value->image_link; ?>" height="260">
               		</a>
                </div>
                <div class="info-more">
              		<p class="des-more"><a href="<?php echo base_url($value->slug_catalog->slug.'/'.$value->slug.'.html')?>"><?php echo $value->name; ?></a></p>
              	</div>
              	<div class="by-more">
              		<p>
						<?php if(isset($value->slug_trademake->name)) { ?>
						<?php echo $value->slug_trademake->name ;?>
						<?php } ?>
					</p>
              	</div>
              	<div class="more-price">
              		<p><?php echo ($product->price != 0 ) ? number_format($product->price).' ₫' : 'Giá Liên Hệ';?> </p>
            	 </div>
            </div>
			<?php } } ?>


     
        </div>
	</div>
</div>
