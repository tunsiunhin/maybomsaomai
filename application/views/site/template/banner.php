<!-- Banner Vertical Menu -->
    <div class="container">
      <div class="banner-content clearfix">
        <?php $this->load->view('site/template/verticalmenu_dashboard') ?>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9" style="padding-right:0">
         <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
              <div class="item active">
                  <img src="//bizweb.dktcdn.net/100/069/405/themes/93985/assets/slide-img1.jpg?1524302933135" alt="Los Angeles">
                </div>
                <!--<div class="item">
                  <img src="//bizweb.dktcdn.net/100/069/405/themes/93985/assets/slide-img5.jpg?1524302933135" alt="Chicago">
                </div>-->

                <div class="item">
                  <img src="//bizweb.dktcdn.net/100/069/405/themes/93985/assets/slide-img1.jpg?1524302933135" alt="New York">
                </div>
              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>

            <!-- Mini Slider -->
             <div class="owl-carousel owl-mini owl-theme">
                  <div class="item-mini">
                    <a href="#">
                      <img class="img-responsive" src="/assets/miniBanner/ebara.png" height="260">
                    </a>
                  </div>
                   <div class="item-mini">
                    <a href="#">
                      <img class="img-responsive" src="/assets/miniBanner/pentax.png" height="260">
                    </a>
                  </div>
                   <div class="item-mini">
                    <a href="#">
                      <img class="img-responsive" src="/assets/miniBanner/cnp.png" height="260">
                    </a>
                  </div>
                   <div class="item-mini">
                    <a href="#">
                       <img class="img-responsive" src="/assets/miniBanner/simghe.png" height="260">
                    </a>
                  </div>
                   <div class="item-mini">
                    <a href="#">
                      <img class="img-responsive" src="/assets/miniBanner/hitachi.png" height="260">
                    </a>
                  </div>
                   <div class="item-mini">
                    <a href="#">
                       <img class="img-responsive" src="/assets/miniBanner/panasonic.png" height="260">
                    </a>
                  </div> 
                  <div class="item-mini">
                    <a href="#">
                       <img class="img-responsive" src="/assets/miniBanner/wilo.png" height="260">
                    </a>
                  </div>              
              </div>
            <!-- End Mini Slider -->
        </div>
        <!-- /.col-sm-9 -->

      </div>
      <!-- /.row -->

    </div>
