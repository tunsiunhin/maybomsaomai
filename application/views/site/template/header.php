<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="maybomsaomai">
    <title>Máy Bơm Nước Sao Mai</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="<?php echo base_url()?>assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/main.js?v=<?=time()?>"></script>
  </head>
  <body style="background:#f5f5f5;">
    <div class="container clearfix">
         <div class="logo-bar clearfix">
           <div class="logo-left"><a class="navbar-brand" href="/home">CÔNG TY CỔ PHẦN ĐẦU TƯ VÀ THƯƠNG MẠI SAO MAI VIỆT NAM</a></div>
           <div class="search-bar">
             <div class="search-container">
                <input class="search-input" type="text" placeholder="Tìm Kiếm ..." name="search">
                <button type="button"><i class="fas fa-search"></i></button>
             </div>
           </div>
         </div>
    </div>

    <div class="container clearfix">
      <div class="menu" >
        <div class="img-logo">
          <a href="/home"><img class="img-responsive" width="150" src="<?php echo base_url().'assets/img/logo.png'?>"></a>
        </div>
        <ul class="main-menu clearfix">
          <li><a href="/home"><i class="fas fa-home"></i> Home</a></li>
          <li><a href="/product"> Sản Phẩm</a></li>
          <li><a href="/gioithieu">Giới Thiệu</a></li>
          <li><a href="/chinhsachgiaohang">Chính Sách Giao Hàng</a></li>
          <li><a href="/lienhe">Liên Hệ [HotLine 0916131419]</a></li>
      </ul>
      </div>
      
    </div>