<div class="container">
	<div class="row">
		 <div class="col-md-12">
		 	<div class="page-header" style="margin:10px 0">
			<h4>Tìm Kiếm Cho : <?php echo $query ? $query :''; ?></h4>      
		 </div>
		 </div>
	</div>
</div>
<div class="container-fluild list-product-search" >
		<div class="container">
			<div class="list-product clearfix">
				<div class="col-md-12">
					<?php foreach ($product as $key => $value){
						$price = ($value['price'] != 0) ? number_format($value['price']).' ₫' : 'Giá Liên Hệ';
						echo '<div class="col-md-3">
						<div class="single-product">
							<a href="'.$value['slug'].'/'.$value['prdslug'].'.html">
								<img class="img-product" src="'.$value['image_link'].'">
							</a>
							<div class="detail-product">
								<div class="name-product">
									<a href="">'.$value['prdname'].'</a>
								</div>
								<div class="brand-product">
									<p>by '.$value['name'].'</p>
								</div>
								<div class="price-product">
									<span>'.$price.'</span>
								</div>
							</div>
						</div>
					</div>';
					} ?>
				</div>
			</div>
		</div>
		<div class="container">
			<!-- <div class="pagination-area">
				<ul class="pagination">
				  <li><a href="#">1</a></li>
				  <li><a href="#">2</a></li>
				  <li><a href="#">3</a></li>
				  <li><a href="#">4</a></li>
				  <li><a href="#">5</a></li>
				</ul>
			</div>	 -->
		</div>
	</div>