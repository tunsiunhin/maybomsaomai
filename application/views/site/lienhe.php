<div class="container">
	<div class="form-contact">
		<div class="col-md-6">
				  <div class="page-header">
				    <h3>Liên Hệ</h3>      
				  </div>
					  <div class="form-group">
					    <label for="exampleInputEmail1">Họ Và Tên <span style="color:red">*</span></label>
					    <input type="email" class="form-control" id="hoVaTen" aria-describedby="emailHelp" placeholder="Họ và Tên">
					  </div>
					  <div class="form-group">
					    <label for="exampleInputPassword1">Email <span style="color:red">*</span> </label>
					    <input type="text" class="form-control" id="email" placeholder="Email">
					  </div>
					  <div class="form-group">
					    <label for="exampleInputPassword1">Nội Dung <span style="color:red">*</span></label>
					    <textarea rows="4" class="form-control" id="noiDung" placeholder="Nội Dung..."></textarea>
					  </div>
					  <button type="submit" class="btn btn-success btn-send"><span class="glyphicon glyphicon-send	
"></span> Gửi</button>

		</div>
	</div>
</div>
<script >
	$(document).ready(function(){
		$('.btn-send').click(function(){
			let name 	= $('#hoVaTen').val();
			let email 	= $('#email').val();
			let content = $('#noiDung').val();
			if(!name || !email || !content)
			{
				alert('Điền Đầy Đủ Thông Tin Để Gửi Liên Hệ');
				return;
			}
			$.ajax({
				url:'ajax/lienhe',
				type:'post',
				data:{'name':name,'email':email,'content':content},
				beforeSend:function()
				{
					$('.btn-send').prop('disabled',true);
				},
				success:function(res)
				{
					$('.btn-send').prop('disabled',false);
					res = res.trim();
					if(res == 1)
						alert('Chúng Tôi Sẽ Liên Hệ Lại Với Bạn Sớm Nhất');
				}

			})
		});
	});
</script>