<div class="container">
    <?php $id = array();?>
    <?php foreach ($catalog_home as $catalog) { ?>
        <?php $id = array($catalog->id);
        if (!empty($catalog->subs)) {
            foreach ($catalog->subs as $sub) {
                $id[] = $sub->id;
            }
        }
        ?>
        <div class="list-product clearfix">
            <h3 class="detail-titel"> Sản Phẩm Nổi Bật</h3>
            <div class="block-run">
                <div class="col-md-12 pord">
                    <div class="link-pump">
                        <a href="<?php echo '/'.$catalog->slug; ?>"><?php echo $catalog->name; ?></a>
                    </div>
                </div> <!-- End col-md-3 -->
                <div class="col-md-12 products-owl">
                    <div class="owl-carousel owl1 owl-theme">
                        <?php if (!empty($product)) { ?>
                            <?php foreach ($product as $value) { ?>
                                <?php if (in_array($value->catalog_id, $id)) { ?>
                                    <div class="slide-product">
                                        <a href="<?php echo base_url($value->slug_catalog->slug . '/' . $value->slug . '.html') ?>">
                                            <img class="img-responsive" src="<?php echo $value->image_link; ?>"
                                                 alt="<?php echo $value->name ?>">
                                            <p><?php echo $value->name ?></p>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php }
                        } ?>

                    </div>
                </div><!-- End col9 -->
            </div>
        </div>
    <?php } ?>

</div>
