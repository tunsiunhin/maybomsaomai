<!-- <div class="container">
    <div class="cum">
    	<span>Trang Chủ</span> / <span>Sản Phẩm</span>
    </div>
</div> -->
<div class="container-fuild " style="background: white;margin-top:10px">
	<div class="container">
		<div class="list-product clearfix">
			<div class="col-sm-3 vertical-menu vertical-menu-product">
	         	<div class="menu1">
                    <p><i class="fa fa-bars"></i> Sản Phẩm</p>
                    <ul class="item-vertical">
                        <?php if (!empty($catalog_list)) { ?>
                            <?php foreach ($catalog_list as $value) { ?>
                                <li>
                                    <a href="/<?=$value->slug?>"><?=$value->name?></a><span class="show-more-menu show-menu"><i class="fa fa-plus"></i></span>
                                    <ul class="menu-con">
                                       <?php foreach ($value->subs as $v) {
                                          echo  '<li><a href="/'.$v->slug.'">'.$v->name.'</a></li>';
                                       } ?>
                                    </ul>
                                  </li>
                            <?php }
                        } ?>

                    </ul>
                </div>
	           <div class="menu2">
	          	<p><i class="fa fa-bars"></i> Thương Hiệu</p>
		          <ul class="item-vertical">
		          	<?php foreach ($trademarks as $value) { ?>
		          		 <li><a href="<?php echo base_url($value['slug']).'/'?>"><?php echo $value['name'] ?></a></li>
		          	<?php } ?>
		          </ul>
	          </div>
			</div>
			<div class="col-sm-9 all-product">
				<?php if(!empty($buy)) { ?>
				<?php foreach ($buy as $value) { ?>
					<div class="col-md-4">
					<div class="single-product">
						<a href="<?php echo base_url($value->slug_catalog.'/'.$value->slug.'.html')?>">
							<img class="img-product" src="<?php echo $value->image_link; ?>">
						</a>
						<div class="detail-product">
							<div class="name-product">
								<a href="<?php echo base_url($value->slug_catalog.'/'.$value->slug.'.html')?>"><?php echo $value->name; ?></a>
							</div>
							<div class="brand-product">
									<?php if(isset($tm))
									{
										echo '<p> by '.$tm->name.'<p>';
									}else if(isset($value->slug_trademake))
									{
										echo '<p> by '.$value->slug_trademake->name.'<p>';
									}
									?> 
							</div>
							<div class="price-product">
								<span><?php echo ($value->price != 0) ? number_format($value->price).' VNĐ': 'Giá Liên Hệ' ;?></span>
							</div>
						</div>
					</div>
				</div>
				<?php } } ?>
				<div class="col-md-12"> <?php echo $this->pagination->create_links()?> </div>
			</div>
			
		</div>
	</div>
</div>