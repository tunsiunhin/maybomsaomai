<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Main extends MY_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model('catalog_model');
		$this->load->model('trademark_model');
        $this->load->library('pagination');
 	}
	public function dashboard(){

        // get all product

        $input = array();
        $input['order'] = array('created','DESC');
        $products = $this->product_model->get_list($input);
        $product = array();
        foreach ($products as $key => $value) {
            //lay slug catalog cua tung bai viet
            $value->slug_catalog = $this->catalog_model->get_info($value->catalog_id);
            $product[]= $value;
        }
        $this->data['product'] = $product;
//        echo "<pre>";
//        print_r($product);
//        die;
        // echo "<pre>";
        // print_r($pro_new);
        // die;

        $this->load->model('catalog_model');
        $input = array();
        $input['where'] = array('parent_id' => 0, 'status'=> 1);
        $catalog_list = $this->catalog_model->get_list($input);
        foreach ($catalog_list as $row) {
            $input['where'] = array('parent_id' => $row->id);
            $subs = $this->catalog_model->get_list($input);
            $row->subs = $subs;
        }
        $this->data['catalog_home'] = $catalog_list;

        $this->load->view('site/template/header', $this->data);
        $this->load->view('site/template/banner', $this->data);
        $this->load->view('site/dashboard', $this->data);
        $this->load->view('site/template/footer', $this->data);
    }

	public function detail()
	{
		//lay id san pham muon xem
        $slug = $this->uri->rsegment(3);
        $where = array('slug' => $slug);
        $product = $this->product_model->get_info_rule($where);
        // print_r($product);
        // die;

        if(!$product)
        {
            redirect();
        }
        $this->data['product'] = $product;
        // echo"<pre>";
        // print_r($product);
        // die;
        //lấy danh sách ảnh sản phẩm kèm theo
        $image_list = @json_decode($product->image_list);
        $this->data['image_list'] = $image_list;
        //lay thong tin cua danh mục san pham
        $catalog = $this->catalog_model->get_info($product->catalog_id);
        $this->data['catalog'] = $catalog;
        //neu $catalog->parent_id == 0 => day la danh muc cha
        //neu #0 thi suy ra day la danh muc con. can lay ra danh muc cha
        if($catalog->parent_id != 0)
        {
            $where = array('id'=> $catalog->parent_id);
            $parent_catalog = $this->catalog_model->get_info_rule($where);
            $this->data['parent_catalog'] = $parent_catalog;
        }
        /*
        //lấy danh sách sản phẩm liên quan
        */
        $input = array();
        $input['where'] = array( 'trademark_id' => $product->trademark_id);
        if(isset($product->id))
        {
            $this->db->where('id <>', $product->id);//hoặc sử dụng $this->db->where('id !=', $id);
        }
        $input['limit'] = array(2,0);
        $related_posts = $this->product_model->get_list($input);
        $pro_new = array();
        foreach ($related_posts as $key => $value) {
            //lay slug catalog cua tung bai viet
            $value->slug_catalog = $this->catalog_model->get_info($value->catalog_id);
            $value->slug_trademake = $this->trademark_model->get_info($value->trademark_id);
            $pro_new[]= $value;
        }
        $this->data['related_post'] = $pro_new;
        $this->data['related_posts'] = $related_posts;
		$this->load->view('site/template/header', $this->data);
		$this->load->view('site/detail-product', $this->data);
		$this->load->view('site/template/footer', $this->data);
	}
	public function catalog(){
        $redirect = 0;
        $slug_catalog = $this->uri->rsegment(3);
        $slug_catalog = strtolower($slug_catalog);
        if($slug_catalog == 'admin')
        {
            redirect(admin_url('admin'));
        }

        $this->load->model('catalog_model');
        $where= array('slug' => $slug_catalog);
        $catalog = $this->catalog_model->get_info_rule($where);
        if(!$catalog)
        {
            $redirect++;
            
        }
        if($redirect > 0)
        {
           $tm = $this->trademark_model->get_info_rule($where);
           if(!$tm)
            $redirect++; 
        }
       
        if($redirect > 1)
            redirect(base_url());

        /*$this->data['catalog'] = $catalog;*/
    
        $input = array();
        //kiêm tra xem đây là danh con hay danh mục cha
        if(!empty($catalog) && $catalog->parent_id == 0)
        {
            $input_catalog = array();
            $input_catalog['where']  = array('parent_id' => $catalog->id);
            $catalog_subs = $this->catalog_model->get_list($input_catalog);
            $this->data['catalog_subs'] = $catalog_subs;
            if(!empty($catalog_subs)) //neu danh muc hien tai co danh muc con
            {
                $catalog_subs_id = array();
                foreach ($catalog_subs as $sub)
                {
                    $catalog_subs_id[] = $sub->id;
                }
                array_push($catalog_subs_id, $catalog->id);
            }else{
                $input['where'] = array('catalog_id' => $catalog->id);
            }
        }else if(!empty($catalog) && $catalog->parent_id != 0){
            $input['where'] = array('catalog_id' => $catalog->id);
        }
        if(isset($tm))
        {
            $input['where'] = array('trademark_id' => $tm->id);
            $this->data['tm'] = $tm;
        }
        $this->data['trademarks']   =  $this->trademark_model->get_all_trademarks();
        //lấy ra danh sách sản phẩm thuộc danh mục đó
        //lay tong so luong ta ca cac san pham trong websit
        if(isset($catalog_subs_id))
        {
            $this->db->where_in('catalog_id', $catalog_subs_id);
        }
        $list_one = $this->product_model->get_list($input);

        $this->data['total_rows'] = count($list_one);

        //load ra thu vien phan trang
        $this->load->library('pagination');
        $config = array();
        $config['base_url']   = base_url( $slug_catalog.'/page');
        $config['total_rows'] = count($list_one);//tong tat ca cac san pham tren website
        $config['per_page']   = 12 ;//so luong san pham hien thi tren 1 trang
        // $config['base_url']   = base_url('product/catalog/'.$catalog->id);
        // $config['uri_segment'] = 3;
        // echo $config['uri_segment'];

        $config['uri_segment'] = 3;//phan doan hien thi ra so trang tren url
        $config['next_link']   = 'Trang kế tiếp';
        $config['prev_link']   = 'Trang trước';
        //khoi tao cac cau hinh phan trang
        $this->pagination->initialize($config);

        $segment = $this->uri->rsegment(4);
        // $segment = $this->uri->rsegment(4);
        $segment = intval($segment);
        // echo $segment;
        // die();
        $input['limit'] = array($config['per_page'], $segment);
        //lay danh sach san pham
         $buy = array();
            foreach ($list_one as $key => $value) {
                //lay slug catalog cua tung bai viet
                $value->slug_catalog = $this->catalog_model->get_info($value->catalog_id)->slug;
                $value->slug_trademake = $this->trademark_model->get_info($value->trademark_id);
                $buy[]= $value;
            }
        $this->data['buy'] = $buy;
        $this->load->view('site/template/header', $this->data);
		$this->load->view('site/catalog', $this->data);
		$this->load->view('site/template/footer', $this->data);
	}
	public function lienhe()
	{
		$this->load->view('site/template/header');
		$this->load->view('site/lienhe');
		$this->load->view('site/template/footer');
	}
	public function send_lienhe()
    {
        $data = $this->input->post();
        if(!$data || !$data['name'] || !$data['email'] || !$data['content'])
            exit('2');
        $data['created'] = time();
        $this->db->insert('contact',$data);
        echo '1';
    }
	public function search()
	{
	   
	    $limit  = 20;
        $q      = $this->input->get('q') ? $this->input->get('q') : '';
        $page   = $this->input->get('page') ? $this->input->get('page') : 0;
        $page   = intval($page);
        $start  = $limit * $page;
        $this->data['query'] = $q;
        $this->db->select('*,product.name as prdname,product.slug as prdslug');
        if($q)
        {
            $this->db->like('product.name',$q);
        }
        $this->db->join('trademarks','trademarks.id = product.trademark_id');
        $this->data['product'] = $this->db->get('product',$limit,$start)->result_array();
      /*  echo '<pre>';
        print_r($this->data);
        die;*/
		$this->load->view('site/template/header');
		$this->load->view('site/timkiem',$this->data);
		$this->load->view('site/template/footer');
	}
	public function chinhsachgiaohang()
	{
		$this->load->view('site/template/header');
		$this->load->view('site/chinhsachgiaohang');
		$this->load->view('site/template/footer');
	}
}
?>
