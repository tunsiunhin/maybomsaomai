<?php
Class Product extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        //load ra file model
        $this->load->model('product_model');
        $this->load->model('trademark_model');

    }
    
    /*
     * Hien thi danh sach san pham
     */
    function index()
    {
        //lay tong so luong ta ca cac san pham trong websit
        $total_rows = $this->product_model->get_total();
        $this->data['total_rows'] = $total_rows;
        
        //load ra thu vien phan trang
        $this->load->library('pagination');
        $config = array();
        $config['total_rows'] = $total_rows;//tong tat ca cac san pham tren website
        $config['base_url']   = admin_url('product/index'); //link hien thi ra danh sach san pham
        $config['per_page']   = 15;//so luong san pham hien thi tren 1 trang
        $config['uri_segment'] = 4;//phan doan hien thi ra so trang tren url
        $config['next_link']   = 'Trang kế tiếp';
        $config['prev_link']   = 'Trang trước';
        //khoi tao cac cau hinh phan trang
        $this->pagination->initialize($config);
        
        $segment = $this->uri->segment(4);

        $segment = intval($segment);
        
        $input = array();
        $input['limit'] = array($config['per_page'], $segment);

        //kiem tra co thuc hien loc du lieu hay khong
        $id = $this->input->get('id');
        $id = intval($id);
        $input['where'] = array();
        if($id > 0)
        {
            $input['where']['id'] = $id;
        }
        $name = $this->input->get('name');
        if($name)
        {
            $input['like'] = array('name', $name);
        }
        $catalog_id = $this->input->get('catalog');
        $catalog_id = intval($catalog_id);

        if($catalog_id > 0)
        {
            $input['where']['catalog_id'] = $catalog_id;
        }
        
        //lay danh sach san pham theo ngày
        
        $input['order'] = array('created','DESC');
        $list_one = $this->product_model->get_list($input);
            $buy = array();
            foreach ($list_one as $key => $value) {
                //lay slug catalog cua tung bai viet
                $this->load->model('catalog_model');
                $value->slug_catalog = $this->catalog_model->get_info($value->catalog_id)->slug;
                $buy[]= $value;
            }
            $this->data['buy'] = $buy;

        
       
        //lay danh sach danh muc san pham
        $this->load->model('catalog_model');
        $input = array();
        $input['where'] = array('parent_id' => 0);
        $catalogs = $this->catalog_model->get_list($input);
        foreach ($catalogs as $row)
        {
            $input['where'] = array('parent_id' => $row->id);
            $subs = $this->catalog_model->get_list($input);
            $row->subs = $subs;
        }
        $this->data['catalogs'] = $catalogs;
        
        //lay nội dung của biến message
        $message = $this->session->flashdata('message');
        $this->data['message'] = $message;
        
        //load view
        $this->data['temp'] = 'admin/product/index';
        $this->load->view('admin/main', $this->data);
    }
    
    /*
     * Them san pham moi
     */
    function add()
    {
        //lay danh sach danh muc san pham
        $this->load->model('catalog_model');
        $input = array();
        $input['where'] = array('parent_id' => 0);
        $catalogs = $this->catalog_model->get_list($input);
        foreach ($catalogs as $row)
        {
            $input['where'] = array('parent_id' => $row->id);
            $subs = $this->catalog_model->get_list($input);
            $row->subs = $subs;  
        }
        $this->data['catalogs'] = $catalogs;
        
        //lay danh sach thương hiệu
        $input['where'] = array('id' !=  0);
        $trademarks = $this->trademark_model->get_list($input);
        $this->data['trademarks'] = $trademarks;

        //load thư viện validate dữ liệu
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        //neu ma co du lieu post len thi kiem tra
        if($this->input->post())
        {
            $this->form_validation->set_rules('name', 'Tên', 'required');
            $this->form_validation->set_rules('catalog', 'Thể loại', 'required');
            $this->form_validation->set_rules('trademark', 'Thương hiệu', 'required');
            if($this->input->post('slug') != '')
                $this->form_validation->set_rules('slug', 'Slug', 'callback__check_slug');
                
            //nhập liệu chính xác
            if($this->form_validation->run())
            {
                //them vao csdl
                $name        = $this->input->post('name');
                $catalog_id  = $this->input->post('catalog');
                $trademark_id = $this->input->post('trademark');
                $price       = $this->input->post('price');
                $price       = str_replace(',', '', $price);
                $thong_so    = $this->input->post('thong_so');


                //luu du lieu can them
                $data = array(
                    'name'       => $name,
                    'catalog_id' => $catalog_id,
                    'trademark_id' => $trademark_id,
                    'price'      => $price,
                    'image_link' => $this->input->post('image'),
                    'image_list' => json_encode($this->input->post('image_list')),
                    'status'     => 'còn hàng',
                    'site_title' => $this->input->post('site_title'),
                    'meta_desc'  => $this->input->post('meta_desc'),
                    'meta_key'   => $this->input->post('meta_key'),
                    'noi_bat'    => $this->input->post('noi_bat'),
                    'video'     => $this->input->post('video'),
                    'content'    => $this->input->post('content'),
                    'thongsokythuat' => $thong_so,
                    'created'    => now(),

                );
                if($this->input->post('slug') == '')
                    $data['slug']  = str_slug($name);
                else
                     $data['slug'] =$this->input->post('slug');

                //them moi vao csdl
                if($this->product_model->create($data))
                {
                    //tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Thêm mới dữ liệu thành công');
                }else{
                    $this->session->set_flashdata('message', 'Không thêm được');
                }
                //chuyen tới trang danh sách
                redirect(admin_url('product'));
            }
        }
        
        
        //load view
        $this->data['temp'] = 'admin/product/add';
        $this->load->view('admin/main', $this->data);
    }
    
    /*
     * Chinh sua san pham
     */
    function edit()
    {
        
        
        if($this->getPermission() != 1)
        redirect(admin_url('permission/deny'));

        $id = $this->uri->rsegment('3');
        $product = $this->product_model->get_info($id);
        if(!$product)
        {
            //tạo ra nội dung thông báo
            $this->session->set_flashdata('message', 'Không tồn tại sản phẩm này');
            redirect(admin_url('product'));
        }
        $this->data['product'] = $product;
       
        //lay danh sach danh muc san pham
        $this->load->model('catalog_model');
        $input = array();
        $input['where'] = array('parent_id' => 0);
        $catalogs = $this->catalog_model->get_list($input);
        foreach ($catalogs as $row)
        {
            $input['where'] = array('parent_id' => $row->id);
            $subs = $this->catalog_model->get_list($input);
            $row->subs = $subs;
        }
        $this->data['catalogs'] = $catalogs;

        //lay danh sach thương hiệu
        $input['where'] = array('id' !=  0);
        $trademarks = $this->trademark_model->get_list($input);
        $this->data['trademarks'] = $trademarks;
        
        //load thư viện validate dữ liệu
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        //neu ma co du lieu post len thi kiem tra
        if($this->input->post())
        {
            $this->form_validation->set_rules('name', 'Tên', 'required');
            $this->form_validation->set_rules('catalog', 'Thể loại', 'required');
            $this->form_validation->set_rules('trademark', 'Thể loại', 'required');
            //$this->form_validation->set_rules('price', 'Giá', 'required');
             if($this->input->post('slug') != '')
                $this->form_validation->set_rules('slug', 'Slug', 'callback__check_slug');
                
        
            //nhập liệu chính xác
            if($this->form_validation->run())
            {
                //them vao csdl
                $name        = $this->input->post('name');
                $catalog_id  = $this->input->post('catalog');
                $trademark_id = $this->input->post('trademark');
                $price       = $this->input->post('price');
                $price       = str_replace(',', '', $price);
                $hsx         = $this->input->post('hsx');
                //$discount    = $this->input->post('discount');
                //$discount    = str_replace(',','', $discount);
                $ma_sp       = $this->input->post('ma_sp');
                $status      = $this->input->post('status');
                //$tong_quan   = $this->input->post('tong_quan');
                $thong_so    = $this->input->post('thong_so');
                //$content   = $this->input->post('content');
                $data = array(
                    'name'       => $name,
                    'catalog_id' => $catalog_id,
                    'trademark_id' => $trademark_id,
                    'price'      => $price,
                    'status'     => $status,
                    'image_list' => json_encode($this->input->post('image_list')),
                    'image_link' => $this->input->post('image'),
                    'gifts'      => $this->input->post('gifts'),
                    'site_title' => $this->input->post('site_title'),
                    'meta_desc'  => $this->input->post('meta_desc'),
                    'meta_key'   => $this->input->post('meta_key'),
                    'noi_bat'    => $this->input->post('noi_bat'),
                    'video'     => $this->input->post('video'),
                    'thongsokythuat' => $thong_so,
                    'content'    => $this->input->post('content'),
                    'created'    => now(),
                );
                 if($this->input->post('slug') == '')
                    $data['slug']  = str_slug($name);
                else
                     $data['slug'] =$this->input->post('slug');
                //them moi vao csdl
                if($this->product_model->update($product->id, $data))
                {
                    //tạo ra nội dung thông báo
                    $this->session->set_flashdata('message', 'Cập nhật dữ liệu thành công');
                }else{
                    $this->session->set_flashdata('message', 'Không cập nhật được');
                }
                //chuyen tới trang danh sách
                redirect(admin_url('product'));
            }
        }
        
        
        //load view
        $this->data['temp'] = 'admin/product/edit';
        $this->load->view('admin/main', $this->data);
    }
    
    /*
     * Xoa du lieu
     */
    function del()
    {
        $id = $this->uri->rsegment('3');
        $this->_del($id);

        
        //tạo ra nội dung thông báo
        $this->session->set_flashdata('message', 'không tồn tại sản phẩm này');
        redirect(admin_url('product'));
    }
    
    /*
     * Xóa nhiều sản phẩm
     */
    function delete_all()
    {
        $ids = $this->input->post('ids');
        foreach ($ids as $id)
        {
            $this->_del($id);
        }
    }
    
    /*
     *Xoa san pham
     */
    private function _del($id)
    {
      if($this->getPermission() != 1)
        redirect(admin_url('permission/deny'));

        $product = $this->product_model->get_info($id);
        if(!$product)
        {
            //tạo ra nội dung thông báo
            $this->session->set_flashdata('message', 'Xóa dữ liệu thành công');
            redirect(admin_url('product'));
        }
        //thuc hien xoa san pham
        $this->product_model->delete($id);
    }
    function _check_slug()
    {
          $slug = $this->input->post('slug');
          $info = $this->product_model->get_info($this->uri->rsegment(3));
          if($this->uri->rsegment('3')){
           $conditional = $this->product_model->get_list(array('where'=>array('slug !=' =>$info->slug,'slug'=>$slug)));
    
          }
          else{
            $conditional = $this->product_model->get_list(array('where'=>array('slug'=>$slug)));
          }

          if($conditional){
            $this->form_validation->set_message(__FUNCTION__,'Slug đã tồn tại!');
            return false;
            }
          else{
            return true;
          }

    }


    //Update giá san pham

        function update()
        {
           $id = isset($_POST['id']) ? $_POST['id'] : false;
           $price = isset($_POST['price']) ? (int)$_POST['price'] : false;
           $data = array();
           $data['price'] = $price;
           $this->product_model->update($id,$data);
           redirect(admin_url('product/index'));


        }
    
}



