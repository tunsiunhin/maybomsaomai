<?php
Class Home extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
        if($this->getPermission() != 1)
        redirect(admin_url('permission/deny'));
	}

    function index()
    {
        $this->data['temp'] = 'admin/home/index';
        $this->load->view('admin/main',$this->data);
    }
}